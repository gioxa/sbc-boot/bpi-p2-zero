

# Enable g_serial USB OTG console

## devicetree


make sure we got for `usb@1c19000`:

```
                        dr_mode = "otg";
                        status = "okay";
```

## kernal modules

Unlike other doc's found, we need to first load `usb_f_acm` before `g_serial` other wise we got `g_serial musb-hdrc.2.auto: failed to start g_serial: -13` and the systemd service for ttyGS0 fails.

```
cat > /etc/modules-load.d/g_serial.conf <<EOF
usb_f_acm
g_serial
EOF
```

## systemd

and thus we only need after reboot of device tree and loading of modules:

```bash
systemctl enable serial-getty@ttyGS0.service
```

followed by

```bash
systemctl start serial-getty@ttyGS0.service
```

## references

http://0pointer.de/blog/projects/serial-console.html

https://linux-sunxi.org/USB_Gadget


## original article for reference

```yaml
from: Jaret Burkett on November 28, 2016 
title: Enable g_serial USB OTG console Orange Pi on Armbian
url: https://oshlab.com/enable-g_serial-usb-otg-console-orange-pi-armbian/
```

There are a lot of things I like about the Next Thing Co’s C.H.I.P. One is that the usb port works as a USB serial console. This is a little tutorial of how to setup the USB OTG g_serial console on all Allwinner H3/H2+ boards including the Orange Pi, Nano Pi, and Banana Pi variants of this chipset.

This may work with other OS's, but it is designed for Armbian. Armbian is amazing when it comes to these Allwinner H3 core boards. Nothing else even comes close.

All of these codes need to be run as root, so well enter super user mode.
```	
sudo su
```

First thing you need to do is add the g_serial to the modules. Run the following command.

```	
echo "g_serial" >> /etc/modules
```

We may or may not need to add a directory, the following code will do that for you if it doesn’t exist
	
```
mkdir -pv /etc/systemd/system/serial-getty@ttyGS0.service.d
```

now we need to create a file in that directory

```	
nano /etc/systemd/system/serial-getty@ttyGS0.service.d/10-switch-role.conf
```

In that file, add the following lines. 

---

---

```
[Service]
ExecStartPre=-/bin/sh -c "echo 2 > /sys/bus/platform/devices/sunxi_usb_udc/otg_role"
```

exit that by pressing Ctrl+x, the tap y, then press enter. Run the following commands.
```
systemctl --no-reload enable serial-getty@ttyGS0.service
echo "ttyGS0" >> /etc/securetty
```

That is it. Just give it a reboot.
```	
reboot
```

Now, if you are connected to a computer via USB through the OTG port, a serial port will appear which will allow you to access the console without having to have a usb/serial converter.

That’s all folks.

# BPI-P2 ZERO / MAKER

## diferences

|P2 Maker| P2 Zero|
|---|---|
|![pic/BPI-P2 MAKER.png](./pic/BPI-P2 MAKER.png)|![pic/BPI-P2-Zero.png](./pic/BPI-P2-Zero.png)|
|- FLASH |+ FLASH|
|- WIFI  |+ WIFI |

## SOFTWARE 

### Image

From [Centos SpecialInterestGroup armhfp](https://wiki.centos.org/SpecialInterestGroup/AltArch/armhfp)

download from [isos centos armhfp](http://isoredirect.centos.org/altarch/7/isos/armhfp) the generic Minimal iso.

and burn on sd-card, minimun 4GB:

```bash
xzcat CentOS-Userland-7-armv7hl-generic-Minimal-$RELEASE-sda.raw.xz | sudo dd of=$/path/to/sd/card status=progress bs=4M
sync
```

### u-boot

Install on a `x64 centos7` u-boot tools

``` bash
sudo yum install uboot-images-armv7 uboot-images-armv8
```

and burn 

```bash
export sdcard="/dev/mmcblk0"
dd if=/usr/share/uboot/bananapi_m2_zero/u-boot-sunxi-with-spl.bin of=${sdcard} bs=1024 seek=8 conv=fsync,notrunc
```

OR copy and burn

```bash
export sdcard="/dev/mmcblk0"
scp c7-u-boot-tools:/usr/share/uboot/bananapi_m2_zero/u-boot-sunxi-with-spl.bin u-boot-sunxi-with-spl.bin
dd if=u-boot-sunxi-with-spl.bin of=${sdcard} bs=1024 seek=8 conv=fsync,notrunc
```

OR use this repo

```bash
git clone https://gitlab.com/igcgroup/logit/bpi-p2-zero
dd if=bpi-p2-zero/u-boot-sunxi-with-spl.bin of=${sdcard} bs=1024 seek=8 conv=fsync,notrunc
```

### patch Device Tree

Problem on for BPI-M2 Zero has `no network` device

#### Boot & login

Boot the SD card with display/keyboard or serial on serial 0 with ttl-serial to usb and computer.
with `root:centos`

#### Decompile the Device Tree binary
```bash
cp /boot/dtb-$(uname -r)/sun8i-h2-plus-bananapi-m2-zero.dtb /boot/dtb-$(uname -r)/sun8i-h2-plus-bananapi-m2-zero.dtb.org
dtc -I dtb -O dts -o sun8i-h2-plus-bananapi-m2-zero.dts  /boot/dtb-$(uname -r)/sun8i-h2-plus-bananapi-m2-zero.dtb
```

#### Patch with diff
```diff
diff --git a/sun8i-h2-plus-bananapi-m2-zero.dts b/sun8i-h2-plus-bananapi-m2-zero_1.dts
index ae013b8..2a68627 100644
--- a/sun8i-h2-plus-bananapi-m2-zero.dts
+++ b/sun8i-h2-plus-bananapi-m2-zero_1.dts
@@ -218,7 +218,7 @@
                         phys = <0xd 0x0>;
                         phy-names = "usb";
                         extcon = <0xd 0x0>;
-                       dr_mode = "otg";
+                        dr_mode = "host";
                         status = "okay";
                 };
 
@@ -433,11 +433,13 @@
                         uart2-pins {
                                 pins = "PA0", "PA1";
                                 function = "uart2";
+                                phandle = <0xfd>;
                         };
 
                         uart3-pins {
                                 pins = "PA13", "PA14";
                                 function = "uart3";
+                                phandle = <0xfe>;
                         };
 
                         uart3-rts-cts-pins {
@@ -464,6 +466,9 @@
                         clocks = <0x3 0x1b>;
                         clock-names = "stmmaceth";
                         status = "okay";
+                        phy-handle = <0xff>;
+                        phy-mode = "mii";
+                        allwinner,leds-active-low;
 
                         mdio {
                                 #address-cells = <0x1>;
@@ -489,6 +494,7 @@
                                                 reg = <0x1>;
                                                 clocks = <0x3 0x43>;
                                                 resets = <0x3 0x27>;
+                                                phandle = <0xff>;
                                         };
                                 };
 
@@ -629,17 +635,6 @@
                         pinctrl-names = "default";
                         pinctrl-0 = <0x18 0x19>;
                         uart-has-rtscts;
-
-                       bluetooth {
-                               compatible = "brcm,bcm43438-bt";
-                               clocks = <0x10 0x1>;
-                               clock-names = "lpo";
-                               vbat-supply = <0x9>;
-                               vddio-supply = <0x9>;
-                               device-wakeup-gpios = <0xa 0x6 0xd 0x0>;
-                               host-wakeup-gpios = <0xa 0x6 0xb 0x0>;
-                               shutdown-gpios = <0xa 0x6 0xc 0x0>;
-                       };
                 };
 
                 serial@1c28800 {
@@ -652,7 +647,9 @@
                         resets = <0x3 0x33>;
                         dmas = <0x13 0x8 0x13 0x8>;
                         dma-names = "rx", "tx";
-                       status = "disabled";
+                        status = "okay";
+                        pinctrl-names = "default";
+                        pinctrl-0 = <0xfd>;
                 };
 
                 serial@1c28c00 {
@@ -665,7 +662,9 @@
                         resets = <0x3 0x34>;
                         dmas = <0x13 0x9 0x13 0x9>;
                         dma-names = "rx", "tx";
-                       status = "disabled";
+                        status = "okay";
+                        pinctrl-names = "default";
+                        pinctrl-0 = <0xfe>;
                 };
 
                 i2c@1c2ac00 {
@@ -1043,6 +1042,9 @@
         aliases {
                 serial0 = "/soc/serial@1c28000";
                 serial1 = "/soc/serial@1c28400";
+                serial2 = "/soc/serial@1c28800";
+                serial3 = "/soc/serial@1c28c00";
+                ethernet0 = "/soc/ethernet@1c30000";
         };
 
         leds {
```

#### And compile the device Tree
```bash
dtc -I dts -O dtb -o  /boot/dtb-$(uname -r)/sun8i-h2-plus-bananapi-m2-zero.dtb sun8i-h2-plus-bananapi-m2-zero_1.dts
```

#### reboot

## finetune

### install epel

```
cat > /etc/yum.repos.d/epel.repo << EOF
[epel]
name=Epel rebuild for armhfp
baseurl=https://armv7.dev.centos.org/repodir/epel-pass-1/
enabled=1
gpgcheck=0
EOF
yum clean all
reboot
```
### bonjour/mDNS

```
yum install avahi nss-mdns
systemctl start avahi-daemon
systemctl enable avahi-daemon
firewall-cmd --permanent --add-port=5353/udp
systemctl reload firewalld
```

### non root user

- copies root auth keys to new user, user login will be same a name,
- user need to change password
- no root login
- no password login

```bash
export ssh_user=dgoo2308
adduser "$ssh_user"
mkdir -pv /home/$ssh_user/.ssh
chmod 700 /home/$ssh_user/.ssh
cp -fv /root/.ssh/authorized_keys /home/$ssh_user/.ssh/authorized_keys
chmod 600 /home/$ssh_user/.ssh/authorized_keys
# change owner of the ssh dir to the user
chown -R $ssh_user /home/$ssh_user/.ssh
# restore selinux context for .ssh
restorecon -r -v -F /home/$ssh_user/.ssh
# add new user to sudoers
echo " $ssh_user ALL=(ALL)   ALL" >> /etc/sudoers
# set pswd = user
echo "$ssh_user:$ssh_user" | chpasswd
# force user to change passwd
chage -d 0 $ssh_user

#disable root Login
sed -ri.bak "s/#?PermitRootLogin .*/PermitRootLogin no/" /etc/ssh/sshd_config

# disable password authentification
sed -ri.bak "s/#?PasswordAuthentication .*/PasswordAuthentication no/" /etc/ssh/sshd_config

# change listen port sshd
#sed -ri.bak "s/#?Port .*/Port $ssh_port/" /etc/ssh/sshd_config
#semanage port -a -t ssh_port_t -p tcp $ssh_port

systemctl restart sshd

firewall-cmd --permanent --add-port=22/tcp

echo "color prompt red for root"
cat >> /etc/bashrc << \EOF
if [ $(id -u) -eq 0 ];
then # you are root, make the prompt red
   export PS1="\[\033[01;31m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[01;31m\]#\[\033[0m\] "
else
  export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
fi
EOF
export PS1="\[\033[01;31m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[01;31m\]#\[\033[0m\] "

```

### cockpit

```bash
yum install cockpit
```

Once the installation is complete, you must start and enable the Cockpit service with the following two commands:

```bash
sudo systemctl start cockpit
​sudo systemctl enable cockpit.socket
```

Finally, let's make sure the firewall is open. Out of the box, you shouldn't have any problem reaching Cockpit, but on the off-chance you do, you can open up the firewall with the following commands:

```bash
sudo firewall-cmd --add-service=cockpit
​sudo firewall-cmd --add-service=cockpit --permanent
​sudo firewall-cmd --reload
```

### time

```bash
timedatectl set-timezone Asia/Bangkok
```

```bash
yum install chrony
systemctl enable chronyd
```

in `/etc/chrony.conf` change the server section to a local time server:

```bash
server 192.168.10.195 iburst
server nodered-1.local iburst
server nodered-1.ssltbivdc.com iburst
```

```bash
systemctl start chronyd
chronyc tracking
chronyc sources
```

## hardware

![image_2.png](./pic/connector40p.png)


| GPIO Pin Name | Default Function | BPI-Scale |
|---------------|------------------|----------------|
| CON2-P01      | VCC-3V3      | **OLED-VCC**   |
| CON2-P03      | TWI0-SDA     | **OLED-SDA**   |
| CON2-P05      | TWI0-SCK     | **OLED-SCK**   |
| CON2-P06      | GND          | **OLED-GND**   |
| CON2-P08      | UART3-TX     | **RS232-TX**   |
| CON2-P09      | GND          | **RS232-GND**  |
| CON2-P10      | UART3-RX     | **RS232-RX**   |
| CON2-P11      | UART2-RX     | **Barcode-RX** |
| CON2-P13      | UART2-TX     | **Barcode-TX** |
| CON2-P14      | GND          | **Barcode-GND**|
| CON2-P17      | VCC-3V3      | **RS232-Barcode-3V3**|
| CON2-P37      | PA17-EINT17  | **Rotary_button**|
| CON2-P38      | PA21-EINT21  | **Rotary-pin_b** |
| CON2-P39      | GND          | **GND**          |
| CON2-P40      | PA20-EINT20  | **Rotary-pin_a** |


## OLED Display

![pic/oled.png](./pic/oled.png)

### Kernel driver

Kernel Driver already present in Centos Kernel build:
```bash
$> ls -la /lib/modules/5.4.56-200.el7.armv7hl/kernel/drivers/video/fbdev/

drwxr-xr-x. 3 root root 4096 Aug 30 14:59 .
drwxr-xr-x. 4 root root 4096 Aug 30 14:59 ..
drwxr-xr-x. 2 root root 4096 Aug 30 14:59 core
-rw-r--r--. 1 root root 6020 Aug  9 10:24 ssd1307fb.ko.xz
-rw-r--r--. 1 root root 3728 Aug  9 10:24 vfb.ko.xz
```

### Device Tree

not so good results but should need a bit more attention.
Problem once connected to the kernel, no userspace manipulation with oled.luma is no longer possible.


see more at [oled_kernel_doc](./oled_kernel_doc.md)
```diff

                i2c@1c2ac00 {
                        compatible = "allwinner,sun6i-a31-i2c";
                        reg = <0x1c2ac00 0x400>;
                        interrupts = <0x0 0x6 0x4>;
                        clocks = <0x3 0x3b>;
                        resets = <0x3 0x2e>;
                        pinctrl-names = "default";
                        pinctrl-0 = <0x1a>;
                        status = "disabled";
                        #address-cells = <0x1>;
                        #size-cells = <0x0>;
+                        ssd1306: oled@3c {
+                                compatible = "solomon,ssd1306fb-i2c";
+                                reg = <0x3c>;
+                                solomon,com-lrremap;
+                                solomon,com-invdir;
+                                solomon,com-offset = <32>;
+                                solomon,lookup-table = /bits/ 8 <0x3f 0x3f 0x3f 0x3f>;
+                                 solomon,heigth = <128>;
+                                 solomon,width = <64>;
                        };
                };
```

## OLED U-boot

TODO: look into it! can load bootsplash in u-boot, kernal no device, app renit when booted=> perfect!

But, need to compile u-boot from centos u-boot tools.... OK, ket's see


## OLED Python luma.oled


[](https://luma-oled.readthedocs.io/en/latest/hardware.html)


connected to `/dev/i2c-0`


| OLED Pin | Name | Remarks     | RPi Pin | BPI-Scale |
|----------|------|-------------|-----------|----------------| 
| 1        | GND  | Ground      | CON2-P06  | **OLED-GND** |
| 2        | VCC  | +3.3V Power | CON2-P01  | **OLED-VCC** |
| 3        | SCL  | Clock       | CON2-P05  | **OLED-SCK** |
| 4        | SDA  | Data        | CON2-P03  | **OLED-SDA** |




### Create groups for IO:
```bash
sudo groupadd gpio
sudo groupadd i2c
```

### load modules on start
```bash
sudo cat > /etc/modules-load.d/myio.conf <<EOF
gpio_keys 
leds_gpio
i2c-dev
#ssd1307fb for display
EOF
```
### Set correct permissions
```bash
sudo cat > /etc/udev/rules.d/99-perm.rules <<EOF
KERNEL=="i2c-0", OWNER="root", GROUP="i2c"
KERNEL=="gpiochip0", OWNER="root", GROUP="gpio"
KERNEL=="gpiochip1", OWNER="root", GROUP="gpio"
EOF
```

### add non root to io group
```bash
sudo adduser dgoo2308 i2c
# sudo adduser dgoo2308 gpio
```
*prefer not to add user to gpio, too dangerous gpio is everithing, i2c/spi is part of it*

### test userland non sudo

**REMARK** *exit and relogin otherwise grouppermissions for user are not propagated*

```bash
i2cdetect -y 0
```

### Python Libs

- lib @ [luma.oled](https://github.com/rm-hull/luma.oled)
- examples @ [luma.examples](https://github.com/rm-hull/luma.examples)

Install dependencies:
```bash
sudo yum install python3-pip python3-devel libjpeg-devel libopenjp2-7 libtiff5 freetype-devel
```

Install Lib
```bash
sudo http_proxy=http://192.168.10.195:8118 https_proxy=https://192.168.10.195:8118 -H pip3 install luma.oled
```

## BARCODE READER

| device| connection |
|---|---|
| ![barcode_pic](./pic/barcode_pic.png) | connected to `/dev/ttyS2` |


|Barcode Pin | Name | Remarks     | RPi Pin | RPi Function  |
|---|----|-----|----|----|
| 2 | VCC | CON2-P17      | **VCC-3V3**          | VCC-3V3        |
| 3 | GND | CON2-P14      | **BARCODE-GND**              | GND            |
| 4 | RXD | CON2-P13      | **BARCODE-TX**         | PA0-EINT0      |
| 5 | TXD | CON2-P11      | **BARCODE-RX**         | PA1-EINT1      |

### other HW connections

|Barcode Pin | Name | Remarks     |
|---|---|----| 
| 9 | Buzzer |    Buzzer output signal  |
| 10 | LED |  openCollector 150mA LED   |
| 12 | TRIGGGER |  Trigger a scan    |



## SCALE SERIAL Connection

![rs232](./pic/rs232.png)

connected to `/dev/ttyS3`

|Barcode Pin | Name | Remarks     | RPi Pin | RPi Function  |
|---|----|-----|----|----|
| 1 | VCC | CON2-P17  | **VCC-3V3**  | VCC-3V3 (share Bacode)        |
| 2 | RXD | CON2-P08  | **RS232-TX** | PA14-EINT14      |
| 3 | TXD | CON2-P10  | **RS232-RX** |  PA13-EINT13      |
| 4 | GND | CON2-P09  | **RS232-GND**      | GND |



## Encoder GPIO Key

![rotary](./pic/rotary.png)

devices registered as inputs or events:

```bash
 ls -la /sys/class/input
lrwxrwxrwx.  1 root root 0 Jan  6  1970 event0 -> ../../devices/platform/gpio_keys/input/input0/event0
lrwxrwxrwx.  1 root root 0 Sep  5 23:47 event1 -> ../../devices/platform/rotary/input/input1/event1
lrwxrwxrwx.  1 root root 0 Jan  6  1970 input0 -> ../../devices/platform/gpio_keys/input/input0
lrwxrwxrwx.  1 root root 0 Sep  5 23:47 input1 -> ../../devices/platform/rotary/input/input1
lrwxrwxrwx.  1 root root 0 Jan  6  1970 mice -> ../../devices/virtual/input/mice
```

### Hardware connections

| Encoder | PIN | RPi Pin  |  Function |
|----|-----|----|----|
| enc-a   | CON2-P40 | PA20-EINT20 | **Rotary-pin_a**  |
| enc-b   | CON2-P38 | PA21-EINT21 | **Rotary-pin_b**  |
| enc-com | CON2-P39 | GND         | **Rotary_GND**    |
| KEY     | CON2-P37 | PA17-EINT17 | **Rotary_button** |
| KEY_com | CON2-P39 | GND         | **Rotary_GND**    |

### Device Tree

```dts

        soc {
        ...

         pinctrl@1c20800 {
         
         ....

                        rotary-pins {
                                pins = "PA21", "PA20";
                                function = "gpio_in";
                                phandle = <0xf7>;
                                bias-pull-up;

                        };
                        rotary-sw-pins {
                                pins = "PA17";
                                function= "gpio_in";
                                phandle = <0xf8>;
                                bias-pull-up;
                        };
         };

        ...
        };


	leds {
              	compatible = "gpio-leds";

                heartbeat_led {
                        label = "bananapi-m2-zero:red:heartbeat";
                        gpios = <0xe 0x0 0xa 0x1>;
                        linux,default-trigger = "heartbeat";
                        status="okay";
                };
        };

               gpio_keys {
                compatible = "gpio-keys";
                autorepeat;
                sw4 {
                        label = "power";
                        linux,code = <0x100>;
                        gpios = <0xe 0x0 0x3 0x1>;
                };
                enter {
                        label = "rotary-switch-ENTER";
                        linux,code = <0x1c>;
                        gpios = <0xa 0x0 0x11 0x1>; /* PA17 active low */
                        pinctrl-names = "default";
                        pinctrl-0 = <0xf8>;
                };
        };
        rotary {
                        compatible = "rotary-encoder";
                        linux,axis = <0>; /* REL_X */
                        rotary-encoder,encoding = "gray";
                        rotary-encoder,relative-axis;
                        pinctrl-names = "default";
                        pinctrl-0 = <0xf7>;
                        gpios = <0xa 0x0 0x15 0x1>, <0xa 0x0 0x14 0x1>; /* PA 21 & 20  is inverted */
        };

```

### Kernel check if device registered

```bash
cat /sys/class/input/input0/device/keys 
28,256

cat /sys/class/input/input0/name
gpio_keys

 cat /sys/class/input/input1/name
rotary

```


*TODO:* **how to set pin pull up**

### test rotary

From Blog [ky-040-rotary-encoder-linux-raspberry-pi](https://blog.ploetzli.ch/2018/ky-040-rotary-encoder-linux-raspberry-pi/)

Install [python-evdev](https://python-evdev.readthedocs.io/en/latest/)
```bash
sudo http_proxy=http://192.168.10.195:8118 https_proxy=https://192.168.10.195:8118 pip3 install evdev
```

`test_rot.py`
```python
# -*- encoding: utf-8 -*-
from __future__ import print_function

import evdev
import select

devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
devices = {dev.fd: dev for dev in devices}

value = 1
print("Value: {0}".format(value))

done = False
while not done:
  r, w, x = select.select(devices, [], [])
  for fd in r:
    for event in devices[fd].read():
      event = evdev.util.categorize(event)
      if isinstance(event, evdev.events.RelEvent):
        value = value + event.event.value
        print("Value: {0}".format(value))
      elif isinstance(event, evdev.events.KeyEvent):
        if event.keycode == "KEY_ENTER" and event.keystate == event.key_up:
          done = True
          break
```

```bash
python3 test_rot.py
```


### TOOLS GPIO

[libgpiod v1.4.x](https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/about/?h=v1.4.x)


There are currently six command-line tools available:

* gpiodetect - list all gpiochips present on the system, their names, labels
               and number of GPIO lines

* gpioinfo   - list all lines of specified gpiochips, their names, consumers,
               direction, active state and additional flags

* gpioget    - read values of specified GPIO lines

* gpioset    - set values of specified GPIO lines, potentially keep the lines
               exported and wait until timeout, user input or signal

* gpiofind   - find the gpiochip name and line offset given the line name

* gpiomon    - wait for events on GPIO lines, specify which events to watch,
               how many events to process before exiting or if the events
               should be reported to the console

Examples:
```bash
    # Read the value of a single GPIO line.
    $ gpioget gpiochip1 23
    0

    # Read two values at the same time. Set the active state of the lines
    # to low.
    $ gpioget --active-low gpiochip1 23 24
    1 1

    # Set values of two lines, then daemonize and wait for a signal (SIGINT or
    # SIGTERM) before releasing them.
    $ gpioset --mode=signal --background gpiochip1 23=1 24=0

    # Set the value of a single line, then exit immediately. This is useful
    # for floating pins.
    $ gpioset gpiochip1 23=1

    # Find a GPIO line by name.
    $ gpiofind "USR-LED-2"
    gpiochip1 23

    # Toggle a GPIO by name, then wait for the user to press ENTER.
    $ gpioset --mode=wait `gpiofind "USR-LED-2"`=1

    # Wait for three rising edge events on a single GPIO line, then exit.
    $ gpiomon --num-events=3 --rising-edge gpiochip2 3
    event:  RISING EDGE offset: 3 timestamp: [    1151.814356387]
    event:  RISING EDGE offset: 3 timestamp: [    1151.815449803]
    event:  RISING EDGE offset: 3 timestamp: [    1152.091556803]

    # Wait for a single falling edge event. Specify a custom output format.
    $ gpiomon --format="%e %o %s %n" --falling-edge gpiochip1 4
    0 4 1156 615459801

    # Pause execution until a single event of any type occurs. Don't print
    # anything. Find the line by name.
    $ gpiomon --num-events=1 --silent `gpiofind "USR-IN"`

    # Monitor multiple lines, exit after the first event.
    $ gpiomon --silent --num-events=1 gpiochip0 2 3 5
```

### evtest

```bash
git clone https://github.com/freedesktop-unofficial-mirror/evtest.git
cd evtest
./autogen.sh
make
sudo make install
```


`evtest /dev/input/event0`
```bash
Input driver version is 1.0.1
Input device ID: bus 0x19 vendor 0x1 product 0x1 version 0x100
Input device name: "gpio_keys"
Supported events:
  Event type 0 (EV_SYN)
  Event type 1 (EV_KEY)
    Event code 28 (KEY_ENTER)
    Event code 256 (BTN_0)
Key repeat handling:
  Repeat type 20 (EV_REP)
    Repeat code 0 (REP_DELAY)
      Value    250
    Repeat code 1 (REP_PERIOD)
      Value     33
Properties:
Testing ... (interrupt to exit)
```

`evtest /dev/input/event1`
```bash
Input driver version is 1.0.1
Input device ID: bus 0x19 vendor 0x0 product 0x0 version 0x0
Input device name: "rotary"
Supported events:
  Event type 0 (EV_SYN)
  Event type 2 (EV_REL)
    Event code 0 (REL_X)
Properties:
Testing ... (interrupt to exit)


```

